
output "sqs_queue" {
  description = "A role to allow k8 pod to access the AWS account"
  value       = var.create ? aws_sqs_queue.job_queue.0 : null
}

output "k8_service_account_job_role" {
  description = "The service account role to allow k8 access to AWS"
  value       =  module.k8_service_account_role.k8_access_role
}
