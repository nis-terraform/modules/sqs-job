# Sqs Job

This module is designed to remove most of the boilerplate/common resource definitions associated
with creating a new job that reads from an SQS queue (subscribed to the portal notification bus).
This module will also provide some variables used by the environment
files (which feed the helm configuration)

# Example

Pretend we have a new job called new-job.  Here are the two main files associated with using this
module.

## newJob.tf (located in the terraform/stack folder)

```tf
module "new_job_queue" {
  source        = "git::https://code.vt.edu/nis-terraform/modules/sqs-job.git"
  globals       = local.sqs_job_globals
  queue_name    = "new-queue"
  dlq_name      = "new-queue-dlq"
  filter_policy = jsonencode({ category = ["NEW-JOB"] })
  job_iam_policies = [
    data.aws_iam_policy_document.policy_1.json,
    data.aws_iam_policy_document.policy_2.json
  ]
}
```

Obviously this relies on things being created elsewhere in either the code (for the appropriate
events being created with a category of NEW-JOB)

Each job that uses an sqs queue will required a basic set of permissions. Using the source_policy_documents
property we can pass a list of additional policy documents that will be merged to the default policies.
This allows us to limit the access of a job while also providing the least permissive permissions possible.


```
data "aws_iam_policy_document" "sqs_role_policy" {
  policy_id = "Base permissions role level policy that allows jobs to get messages from the SQS queue"
  source_policy_documents = var.job_iam_policies

  statement {
    actions = [
      "sqs:ChangeMessageVisibility",
      "sqs:ChangeMessageVisibilityBatch",
      "sqs:DeleteMessage",
      "sqs:DeleteMessageBatch",
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage",
    ]

    resources = [
      aws_sqs_queue.job_queue.arn,
      aws_sqs_queue.dlq.arn,
    ]
  }
}
```

## new_job_env.tf

```tf

module "new_job_env" {
  source = "git::https://code.vt.edu/nis-terraform/modules/env-builder.git"
  env    = var.environment
  env_vars = {
    CRON_SCHEDULE= {
      dev = "*/2 * * * *"
      prod = "*/5 * * * *"
    }
    SNS_TOPIC_ARN = aws_sns_topic.portal_notification_bus.arn
    SQS_QUEUE_URL = module.new_job_queue.sqs_queue.url
    NODE_ENV = var.environment
    SIMLOG_LEVEL = {
      dev = "debug"
      prod = "info"
    }
    ORCHESTRA_DB_CONNECT_STRING = "${var.db_prefixes.pg}.db.es.cloud.vt.edu:5432/orchestra"
    ORCHESTRA_DB_USER = "orchestra_user"
    ORCHESTRA_DB_PASSWORD_NAME = "pgdvlp.db.es.cloud.vt.edu-orchestra_user"
    <!-- If the job needs secrets then add this -->
    VAULT_HEADER                  = local.vault_header
    VAULT_ROLE                    = local.vault_role
    VAULT_SECRET                  = local.vault_secret
  }
}
```

Most of the above is just to provide scenery for this example, but pay close attention to
SQS_QUEUE_URL and EVENT_CACHE_DB_NAME, as those refer to the specific invocation of the
module for new-job.