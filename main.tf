resource "aws_sqs_queue" "dlq" {
  name = local.dlq_name
  count = var.create ? 1 : 0
}

resource "aws_sqs_queue" "job_queue" {
  name = local.queue_name
  count = var.create ? 1 : 0
  redrive_policy = jsonencode({
    deadLetterTargetArn = var.create ? aws_sqs_queue.dlq.0.arn : ""
    maxReceiveCount     = 3
  })
}

resource "aws_sns_topic_subscription" "job_queue" {
  count                = var.create ? 1 : 0
  topic_arn            = var.globals.notification_bus_arn
  protocol             = "sqs"
  endpoint             = var.create ? aws_sqs_queue.job_queue.0.arn : ""
  raw_message_delivery = true
  filter_policy        = var.filter_policy
}

module "k8_service_account_role" {
  source = "git::https://code.vt.edu/nis-terraform/modules/k8-service-account-role.git"

  create                  = var.create
  k8_oidc_provider_url    = var.globals.k8_oidc_provider_url
  k8_namespace            = var.globals.k8_namespace
  k8_service_account_name = local.k8_service_account
}
