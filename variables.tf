variable "globals" {
  description = "Map of values required by every job"
  type = object({
    app_prefix           = string
    identifier           = string
    k8_namespace         = string
    k8_oidc_provider_url = string
    notification_bus_arn = string
  })
}

variable "create" {
  description = "Create the resources for this module"
  type        = bool
  default     = true
}

variable "queue_name" {
  description = "Name of the SQS queue to create associated with the job subscription"
  type        = string
}

variable "dlq_name" {
  description = "Name of the dead letter queue associated with the job subscription"
  type        = string
}

variable "filter_policy" {
  description = "The SNS filter policy , passed through jsonencode()"
  type        = string
}

variable "job_iam_policies" {
  description = "JSON iam policies to attach to role when branch is created"
  type        = list(any)
  default     = []
}

locals {
  queue_name         = "${var.globals.app_prefix}-${var.queue_name}-${var.globals.identifier}"
  dlq_name           = "${var.globals.app_prefix}-${var.dlq_name}-${var.globals.identifier}"
  k8_service_account = "k8-${var.globals.app_prefix}-${var.queue_name}-job-${var.globals.identifier}"
  queue_policy_name  = "${var.queue_name}-AllowK8sAccess"
}
