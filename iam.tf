# Define the resource policy that allows the notification bus to send messages into the SQS queue
data "aws_iam_policy_document" "sns_to_sqs" {
  policy_id = "SNS Policy for controlling access to topic"

  statement {
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "SQS:SendMessage"
    ]

    resources = [
      var.create ? aws_sqs_queue.job_queue.0.arn : ""
    ]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values = [
        var.globals.notification_bus_arn
      ]
    }
  }
}

# Create the resource policy then attach the queue and policy json
resource "aws_sqs_queue_policy" "sqs_to_sqs" {
  count     = var.create ? 1 : 0
  queue_url = var.create ? aws_sqs_queue.job_queue.0.id : ""
  policy    = data.aws_iam_policy_document.sns_to_sqs.json
}

# Define the base permissions role level policy that allows jobs to get messages from the SQS queue
data "aws_iam_policy_document" "sqs_role_policy" {
  policy_id               = "Base permissions role level policy that allows jobs to get messages from the SQS queue"
  source_policy_documents = var.job_iam_policies
  statement {
    actions = [
      "sqs:ChangeMessageVisibility",
      "sqs:ChangeMessageVisibilityBatch",
      "sqs:DeleteMessage",
      "sqs:DeleteMessageBatch",
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage",
    ]

    resources = [
      var.create ? aws_sqs_queue.job_queue.0.arn : "",
      var.create ? aws_sqs_queue.dlq.0.arn : "",
    ]
  }
}
# Create the policy attaching the role and policies
resource "aws_iam_role_policy" "k8_job_role_policy" {
  count = var.create ? 1 : 0
  name  = local.queue_policy_name
  role  = var.create ? module.k8_service_account_role.k8_access_role.id : ""
  # We pass in a list of permissions and combine them into a single policy.
  policy = data.aws_iam_policy_document.sqs_role_policy.json
}
